(function ( $) {

	var greenColor = "green";
	var greenbackgroundColor = "white";

    /* Llamada a google translate */
	$.fn.translate = function(options) {

		var settings = $.extend({
			url: "https://translate.googleapis.com/translate_a/single?client=gtx&sl=es&tl=en&dt=t&q=" 
			+ encodeURI(options.text),
		},options);
    	
    	return $.ajax(settings);
	}
}(jQuery));